﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Users
    {
        public Users()
        {
            Reservation = new HashSet<Reservation>();
        }

        public decimal UserId { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string LastName1 { get; set; }
        public string LastName20 { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime? RegisterDate { get; set; }
        public string BussinessName { get; set; }

        public virtual ICollection<Reservation> Reservation { get; set; }
    }
}
