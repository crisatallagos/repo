﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class ProveedorXReceptor
    {
        public decimal BillId { get; set; }
        public decimal WorkerId { get; set; }

        public virtual Bills Bill { get; set; }
        public virtual Workers Worker { get; set; }
    }
}
