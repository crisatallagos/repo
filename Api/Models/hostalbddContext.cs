﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Api.Models
{
    public partial class hostalbddContext : DbContext
    {
        public hostalbddContext()
        {
        }

        public hostalbddContext(DbContextOptions<hostalbddContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bills> Bills { get; set; }
        public virtual DbSet<Habitaciones> Habitaciones { get; set; }
        public virtual DbSet<Platos> Platos { get; set; }
        public virtual DbSet<Productos> Productos { get; set; }
        public virtual DbSet<ProductsXBill> ProductsXBill { get; set; }
        public virtual DbSet<ProveedorXReceptor> ProveedorXReceptor { get; set; }
        public virtual DbSet<Report> Report { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<RoomsXReservation> RoomsXReservation { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<ServicesXReservation> ServicesXReservation { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<VentasXPlato> VentasXPlato { get; set; }
        public virtual DbSet<Workers> Workers { get; set; }
        public virtual DbSet<WorkersLog> WorkersLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=35.230.179.98;Initial Catalog=hostal-bdd;User ID=sqlserver;Password=admin;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bills>(entity =>
            {
                entity.HasKey(e => e.BillId)
                    .IsClustered(false);

                entity.ToTable("BILLS");

                entity.Property(e => e.BillId)
                    .HasColumnName("BILL_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BillDate)
                    .HasColumnName("BILL_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.TotalBilled)
                    .HasColumnName("TOTAL_BILLED")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<Habitaciones>(entity =>
            {
                entity.HasKey(e => e.RoomId)
                    .IsClustered(false);

                entity.ToTable("HABITACIONES");

                entity.Property(e => e.RoomId)
                    .HasColumnName("ROOM_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Bathrooms)
                    .HasColumnName("BATHROOMS")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Beds)
                    .HasColumnName("BEDS")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.IsAvailible).HasColumnName("IS_AVAILIBLE");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Number)
                    .HasColumnName("NUMBER")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Price)
                    .HasColumnName("PRICE")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<Platos>(entity =>
            {
                entity.HasKey(e => e.DishId)
                    .IsClustered(false);

                entity.ToTable("PLATOS");

                entity.Property(e => e.DishId)
                    .HasColumnName("DISH_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Class)
                    .HasColumnName("CLASS")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Price)
                    .HasColumnName("PRICE")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<Productos>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .IsClustered(false);

                entity.ToTable("PRODUCTOS");

                entity.Property(e => e.ProductId)
                    .HasColumnName("PRODUCT_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Price)
                    .HasColumnName("PRICE")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("QUANTITY")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<ProductsXBill>(entity =>
            {
                entity.HasKey(e => new { e.BillId, e.ProductId })
                    .IsClustered(false);

                entity.ToTable("PRODUCTS_X_BILL");

                entity.Property(e => e.BillId)
                    .HasColumnName("BILL_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("PRODUCT_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Bill)
                    .WithMany(p => p.ProductsXBill)
                    .HasForeignKey(d => d.BillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRODUCTS_RELATIONS_BILLS");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductsXBill)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRODUCTS_RELATIONS_PRODUCTO");
            });

            modelBuilder.Entity<ProveedorXReceptor>(entity =>
            {
                entity.HasKey(e => new { e.BillId, e.WorkerId })
                    .IsClustered(false);

                entity.ToTable("PROVEEDOR_X_RECEPTOR");

                entity.Property(e => e.BillId)
                    .HasColumnName("BILL_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.WorkerId)
                    .HasColumnName("WORKER_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Bill)
                    .WithMany(p => p.ProveedorXReceptor)
                    .HasForeignKey(d => d.BillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PROVEEDO_RELATIONS_BILLS");

                entity.HasOne(d => d.Worker)
                    .WithMany(p => p.ProveedorXReceptor)
                    .HasForeignKey(d => d.WorkerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PROVEEDO_RELATIONS_WORKERS");
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.HasKey(e => e.ReportId)
                    .IsClustered(false);

                entity.ToTable("REPORT");

                entity.Property(e => e.ReportId)
                    .HasColumnName("REPORT_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.File).HasColumnName("FILE");

                entity.Property(e => e.FileName)
                    .HasColumnName("FILE_NAME")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("TYPE")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.WorkerId)
                    .HasColumnName("WORKER_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Worker)
                    .WithMany(p => p.Report)
                    .HasForeignKey(d => d.WorkerId)
                    .HasConstraintName("FK_REPORT_RELATIONS_WORKERS");
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.HasKey(e => e.ReservationId)
                    .IsClustered(false);

                entity.ToTable("RESERVATION");

                entity.Property(e => e.ReservationId)
                    .HasColumnName("RESERVATION_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Discount)
                    .HasColumnName("DISCOUNT")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ReservationEnd)
                    .HasColumnName("RESERVATION_END")
                    .HasColumnType("datetime");

                entity.Property(e => e.ReservationStart)
                    .HasColumnName("RESERVATION_START")
                    .HasColumnType("datetime");

                entity.Property(e => e.ReservationTotal)
                    .HasColumnName("RESERVATION_TOTAL")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.SellDate)
                    .HasColumnName("SELL_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.UserId)
                    .HasColumnName("USER_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.WorkerId)
                    .HasColumnName("WORKER_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Reservation)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_RESERVAT_RELATIONS_USERS");

                entity.HasOne(d => d.Worker)
                    .WithMany(p => p.Reservation)
                    .HasForeignKey(d => d.WorkerId)
                    .HasConstraintName("FK_RESERVAT_RELATIONS_WORKERS");
            });

            modelBuilder.Entity<RoomsXReservation>(entity =>
            {
                entity.HasKey(e => e.RXRId)
                    .IsClustered(false);

                entity.ToTable("ROOMS_X_RESERVATION");

                entity.Property(e => e.RXRId)
                    .HasColumnName("R_X_R_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ReservationId)
                    .HasColumnName("RESERVATION_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.RoomId)
                    .HasColumnName("ROOM_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.RoomsXReservation)
                    .HasForeignKey(d => d.ReservationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ROOMS_X__RELATIONS_RESERVAT");

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.RoomsXReservation)
                    .HasForeignKey(d => d.RoomId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ROOMS_X__RELATIONS_HABITACI");
            });

            modelBuilder.Entity<Services>(entity =>
            {
                entity.HasKey(e => e.ServiceId)
                    .IsClustered(false);

                entity.ToTable("SERVICES");

                entity.Property(e => e.ServiceId)
                    .HasColumnName("SERVICE_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NombreServicio)
                    .HasColumnName("NOMBRE_SERVICIO")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnName("PRECIO")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<ServicesXReservation>(entity =>
            {
                entity.HasKey(e => new { e.ReservationId, e.ServiceId })
                    .IsClustered(false);

                entity.ToTable("SERVICES_X_RESERVATION");

                entity.Property(e => e.ReservationId)
                    .HasColumnName("RESERVATION_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ServiceId)
                    .HasColumnName("SERVICE_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.ServicesXReservation)
                    .HasForeignKey(d => d.ReservationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SERVICES_RELATIONS_RESERVAT");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ServicesXReservation)
                    .HasForeignKey(d => d.ServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SERVICES_RELATIONS_SERVICES");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_USUARIOS")
                    .IsClustered(false);

                entity.ToTable("USERS");

                entity.Property(e => e.UserId)
                    .HasColumnName("USER_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BussinessName)
                    .HasColumnName("BUSSINESS_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName1)
                    .HasColumnName("LAST_NAME1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName20)
                    .HasColumnName("LAST_NAME20")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name1)
                    .HasColumnName("NAME1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name2)
                    .HasColumnName("NAME2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegisterDate)
                    .HasColumnName("REGISTER_DATE")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<VentasXPlato>(entity =>
            {
                entity.HasKey(e => e.DishSellId)
                    .HasName("PK_SELLER_X_DISH")
                    .IsClustered(false);

                entity.ToTable("VENTAS_X_PLATO");

                entity.Property(e => e.DishSellId)
                    .HasColumnName("DISH_SELL_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DishId)
                    .HasColumnName("DISH_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.SellDate)
                    .HasColumnName("SELL_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.SellerId)
                    .HasColumnName("SELLER_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Dish)
                    .WithMany(p => p.VentasXPlato)
                    .HasForeignKey(d => d.DishId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DISH_X_SELL");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.VentasXPlato)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SELLER_ID_X_PLATOS");
            });

            modelBuilder.Entity<Workers>(entity =>
            {
                entity.HasKey(e => e.WorkerId)
                    .IsClustered(false);

                entity.ToTable("WORKERS");

                entity.Property(e => e.WorkerId)
                    .HasColumnName("WORKER_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName1)
                    .HasColumnName("LAST_NAME1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName20)
                    .HasColumnName("LAST_NAME20")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name1)
                    .HasColumnName("NAME1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name2)
                    .HasColumnName("NAME2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .HasColumnName("ROLE")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkersLog>(entity =>
            {
                entity.HasKey(e => e.LogId)
                    .IsClustered(false);

                entity.ToTable("WORKERS_LOG");

                entity.Property(e => e.LogId)
                    .HasColumnName("LOG_ID")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IpAddress)
                    .HasColumnName("IP_ADDRESS")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.LastLogin)
                    .HasColumnName("LAST_LOGIN")
                    .HasColumnType("datetime");

                entity.Property(e => e.Location)
                    .HasColumnName("LOCATION")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.WorkerId)
                    .HasColumnName("WORKER_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Worker)
                    .WithMany(p => p.WorkersLog)
                    .HasForeignKey(d => d.WorkerId)
                    .HasConstraintName("FK_WORKERS_LO_RELATIONS_WORKERS");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
