﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Services
    {
        public Services()
        {
            ServicesXReservation = new HashSet<ServicesXReservation>();
        }

        public decimal ServiceId { get; set; }
        public string NombreServicio { get; set; }
        public decimal? Precio { get; set; }

        public virtual ICollection<ServicesXReservation> ServicesXReservation { get; set; }
    }
}
