﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Workers
    {
        public Workers()
        {
            ProveedorXReceptor = new HashSet<ProveedorXReceptor>();
            Report = new HashSet<Report>();
            Reservation = new HashSet<Reservation>();
            VentasXPlato = new HashSet<VentasXPlato>();
            WorkersLog = new HashSet<WorkersLog>();
        }

        public decimal WorkerId { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string LastName1 { get; set; }
        public string LastName20 { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public virtual ICollection<ProveedorXReceptor> ProveedorXReceptor { get; set; }
        public virtual ICollection<Report> Report { get; set; }
        public virtual ICollection<Reservation> Reservation { get; set; }
        public virtual ICollection<VentasXPlato> VentasXPlato { get; set; }
        public virtual ICollection<WorkersLog> WorkersLog { get; set; }
    }
}
