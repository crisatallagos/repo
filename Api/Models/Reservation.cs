﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Reservation
    {
        public Reservation()
        {
            RoomsXReservation = new HashSet<RoomsXReservation>();
            ServicesXReservation = new HashSet<ServicesXReservation>();
        }

        public decimal ReservationId { get; set; }
        public decimal? WorkerId { get; set; }
        public decimal? UserId { get; set; }
        public DateTime? ReservationStart { get; set; }
        public DateTime? ReservationEnd { get; set; }
        public decimal? ReservationTotal { get; set; }
        public decimal? Discount { get; set; }
        public DateTime? SellDate { get; set; }

        public virtual Users User { get; set; }
        public virtual Workers Worker { get; set; }
        public virtual ICollection<RoomsXReservation> RoomsXReservation { get; set; }
        public virtual ICollection<ServicesXReservation> ServicesXReservation { get; set; }
    }
}
