﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class CreateRoomDTO
    {
        public string Name { get; set; }
        public string Beds { get; set; }
        public decimal? Bathrooms { get; set; }
        public decimal? Number { get; set; }
        public bool? IsAvailible { get; set; }
        public decimal? Price { get; set; }
    }
}
