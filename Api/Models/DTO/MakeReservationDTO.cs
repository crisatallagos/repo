﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class MakeReservationDTO
    {
        public decimal userid { get; set; }
        public decimal workerid { get; set; }
        public int startday { get; set; }
        public int startmonth { get; set; }
        public int startyear { get; set; }
        public int endday { get; set; }
        public int endmonth { get; set; }
        public int endyear { get; set; }
        public decimal total { get; set; }
        public List<int> rooms { get; set; }
    }
}
