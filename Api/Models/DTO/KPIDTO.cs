﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class KPIDTO
    {
        public int? todaytotaldishsellscount { get; set; }
        public decimal? todaytotaldishsellsmoney { get; set; }

        public int? totalusers { get; set; }
        public decimal? totaldishsells { get; set; }
        public int? totaldishsellscount { get; set; }
        public int? totalreservesellscount { get; set; }
        public decimal? totalreservesellsmoney { get; set; }
        public int? owndishsellscount { get; set; }
        public decimal? owndishsellsmoney { get; set; }
        public decimal? ownreservesellsmoney { get; set; }
        public int? ownreservesellscount { get; set; }
        public int? todayroomused { get; set; }
        public int? todayregisteredusers { get; set; }
        public int? todaytotalreservesellscount { get; set; }
        public decimal? todaytotalreservesellsmoney { get; set; }
    }
}
