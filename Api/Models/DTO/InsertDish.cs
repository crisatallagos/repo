﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class InsertDish
    {
        public string clase { get; set; }
        public string name { get; set; }
        public decimal? price { get; set; }
    }
}
