﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class SimpleDate
    {
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
    }
}
