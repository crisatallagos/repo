﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class CreateDishDTO
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public decimal Price { get; set; }
    }
}
