﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class SellDishDTO { 
        public decimal dishid { get; set; }
        public decimal sellerId { get; set; }
    }
}
