﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class RegisterDTO
    {
        public string name1 { get; set; }
        public string name2 { get; set; }
        public string lastName1 { get; set; }
        public string lastName20 { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string bussinessname { get; set; }
        public string password { get; set; }
    }
}
