﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.DTO
{
    public class formattedReserve
    {
        public decimal? reserveId { get; set; }
        public decimal? workerid { get; set; }
        public DateTime? from { get; set; }
        public DateTime? to { get; set; }
        public decimal? total { get; set; }
        public List<Habitaciones> habitaciones { get; set; }

    }
}
