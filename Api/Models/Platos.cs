﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Platos
    {
        public Platos()
        {
            VentasXPlato = new HashSet<VentasXPlato>();
        }

        public decimal DishId { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public decimal? Price { get; set; }

        public virtual ICollection<VentasXPlato> VentasXPlato { get; set; }
    }
}
