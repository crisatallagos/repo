﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Report
    {
        public decimal ReportId { get; set; }
        public string Type { get; set; }
        public decimal? WorkerId { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }

        public virtual Workers Worker { get; set; }
    }
}
