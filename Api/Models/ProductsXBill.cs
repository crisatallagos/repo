﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class ProductsXBill
    {
        public decimal BillId { get; set; }
        public decimal ProductId { get; set; }

        public virtual Bills Bill { get; set; }
        public virtual Productos Product { get; set; }
    }
}
