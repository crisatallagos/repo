﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class VentasXPlato
    {
        public decimal DishSellId { get; set; }
        public decimal DishId { get; set; }
        public decimal SellerId { get; set; }
        public DateTime? SellDate { get; set; }

        public virtual Platos Dish { get; set; }
        public virtual Workers Seller { get; set; }
    }
}
