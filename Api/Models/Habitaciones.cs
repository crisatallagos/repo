﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Habitaciones
    {
        public Habitaciones()
        {
            RoomsXReservation = new HashSet<RoomsXReservation>();
        }

        public decimal RoomId { get; set; }
        public string Name { get; set; }
        public string Beds { get; set; }
        public decimal? Bathrooms { get; set; }
        public decimal? Number { get; set; }
        public bool? IsAvailible { get; set; }
        public decimal? Price { get; set; }

        public virtual ICollection<RoomsXReservation> RoomsXReservation { get; set; }
    }
}
