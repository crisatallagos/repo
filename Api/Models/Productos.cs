﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Productos
    {
        public Productos()
        {
            ProductsXBill = new HashSet<ProductsXBill>();
        }

        public decimal ProductId { get; set; }
        public string Name { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Price { get; set; }

        public virtual ICollection<ProductsXBill> ProductsXBill { get; set; }
    }
}
