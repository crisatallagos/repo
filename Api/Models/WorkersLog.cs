﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class WorkersLog
    {
        public decimal? WorkerId { get; set; }
        public decimal LogId { get; set; }
        public DateTime? LastLogin { get; set; }
        public string IpAddress { get; set; }
        public string Location { get; set; }

        public virtual Workers Worker { get; set; }
    }
}
