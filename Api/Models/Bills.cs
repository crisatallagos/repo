﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class Bills
    {
        public Bills()
        {
            ProductsXBill = new HashSet<ProductsXBill>();
            ProveedorXReceptor = new HashSet<ProveedorXReceptor>();
        }

        public decimal BillId { get; set; }
        public DateTime? BillDate { get; set; }
        public decimal? TotalBilled { get; set; }

        public virtual ICollection<ProductsXBill> ProductsXBill { get; set; }
        public virtual ICollection<ProveedorXReceptor> ProveedorXReceptor { get; set; }
    }
}
