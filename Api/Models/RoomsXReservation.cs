﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class RoomsXReservation
    {
        public decimal ReservationId { get; set; }
        public decimal RoomId { get; set; }
        public decimal RXRId { get; set; }

        public virtual Reservation Reservation { get; set; }
        public virtual Habitaciones Room { get; set; }
    }
}
