﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public partial class ServicesXReservation
    {
        public decimal ReservationId { get; set; }
        public decimal ServiceId { get; set; }

        public virtual Reservation Reservation { get; set; }
        public virtual Services Service { get; set; }
    }
}
