﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore;
using Api.Models.DTO;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Api.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    public class ApiController : ControllerBase
    {
        private hostalbddContext _db = new hostalbddContext();

        private string GetClientIP()
        {
            string ClientIP = "127.0.0.1";
            if (HttpContext.Request.Headers.Keys.FirstOrDefault(x => x == "X-Forwarded-For") != null)
            {
                ClientIP = HttpContext.Request.Headers["X-Forwarded-For"].First();
            }
            return ClientIP;
        }

        public ApiController()
        {
        }

        [HttpGet]
        [Route("MisReservas/{userid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> MisReservas(decimal userid)
        {
            Users user = _db.Users.Where(x => x.UserId == userid).FirstOrDefault();
            if (user == null)
            {
                return new BadRequestObjectResult("UserId Doesn't Exists");
            }
            else
            {
                List<Reservation> reservas = _db.Reservation.Where(x => x.UserId == userid).ToList();
                List<formattedReserve> res = new List<formattedReserve>();
                reservas.ForEach(x =>
                    res.Add(new formattedReserve
                    {
                        workerid = x.WorkerId,
                        reserveId = x.ReservationId,
                        from = x.ReservationStart,
                        to = x.ReservationEnd,
                        total = x.ReservationTotal,
                        habitaciones = _db.RoomsXReservation.Where(y => y.ReservationId == x.ReservationId).Include(z => z.Room).Select(a => a.Room).ToList()
                    })
                );
                return new OkObjectResult(res);
            }
        }

        [HttpGet]
        [Route("GetProfile/{userid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetProfile(decimal userid)
        {
            Users user = _db.Users.Where(x => x.UserId == userid).FirstOrDefault();
            if (user == null)
            {
                return new BadRequestObjectResult("UserId Doesn't Exists");
            }
            else
            {
                return new OkObjectResult(user);
            }
        }

        [HttpPost]
        [Route("Login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Login([FromBody] LoginDTO model)
        {
            Users user = await _db.Users.Where(x => x.Email == model.email && x.Password == model.password).FirstOrDefaultAsync();
            if (user == null || string.IsNullOrWhiteSpace(model.password))
            {
                return new BadRequestObjectResult("Bad Credentials");
            }
            return new OkObjectResult(new { email = user.Email, userid = user.UserId });
        }

        [HttpPost]
        [Route("LoginAdmin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> LoginAdmin([FromBody] LoginDTO model)
        {
            Workers user = await _db.Workers.Where(x => x.Email == model.email && x.Password == model.password).FirstOrDefaultAsync();
            if (user == null)
            {
                return new BadRequestObjectResult("Bad Credentials");
            } else { 
                _db.WorkersLog.Add(new WorkersLog()
                {
                    IpAddress = GetClientIP(),
                    LastLogin = DateTime.UtcNow,
                    Location = "LoginAdmin",
                    WorkerId = user.WorkerId
                });
                _db.SaveChanges();
            }
            return new OkObjectResult(new { email = user.Email, userid = user.WorkerId, role = user.Role, name1 = user.Name1 });
        }



        [HttpPost]
        [Route("SellReserve")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SellReserve([FromBody] SellReserveDTO model)
        {
            try
            {
                Users userx = new Users
                {
                    Address = "",
                    Email = model.newuser,
                    Name1 = model.name,
                    LastName1 = model.lastname,
                    Password = "temporal"

                };
                await _db.Users.AddAsync(userx);
                await _db.SaveChangesAsync();

                Users user = _db.Users.Where(x => x.Email == model.newuser).ToList().LastOrDefault();
                user.Reservation.Add(new Reservation()
                {
                    UserId = user.UserId,
                    WorkerId = model.workerid,
                    ReservationStart = new DateTime(model.startyear, model.startmonth + 1, model.startday),
                    ReservationEnd = new DateTime(model.endyear, model.endmonth + 1, model.endday),
                    ReservationTotal = model.total,
                    SellDate = DateTime.UtcNow,
                    ServicesXReservation = new List<ServicesXReservation>() { }
                });
                _db.SaveChanges();


                List<RoomsXReservation> toInsert = new List<RoomsXReservation>();
                var LastReservId = user.Reservation.Last().ReservationId;
                foreach (int roomid in model.rooms)
                {
                    toInsert.Add(new RoomsXReservation()
                    {
                        RoomId = roomid,
                        ReservationId = LastReservId
                    });
                }
                _db.RoomsXReservation.AddRange(toInsert);
                _db.SaveChanges();

                _db.WorkersLog.Add(new WorkersLog()
                {
                     IpAddress = GetClientIP(),
                     LastLogin = DateTime.UtcNow,
                     Location = "SellReserve",
                     WorkerId = model.workerid
                });
                _db.SaveChanges();
                return new OkObjectResult(new { message = "ok" });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }
        }

        [HttpPost]
        [Route("AlterUserProfile")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AlterUserProfile([FromBody] UserProfileDTO model)
        {
            Users user = await _db.Users.Where(x => x.Email == model.email).FirstOrDefaultAsync();
            user.Email = model.email;
            user.Password = model.password != null ? model.password : user.Password;
            user.Phone = model.phone != null ? model.phone : user.Phone;
            user.Address = model.address != null ? model.address : user.Address;
            await _db.SaveChangesAsync();
            if (user == null)
            {
                return new BadRequestObjectResult("Bad Credentials");
            }
            return new OkObjectResult(model);
        }

        [HttpGet]
        [Route("DeleteDish/{dishid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteDish(decimal dishid)
        {
            Platos res = await _db.Platos.Where(x => x.DishId == dishid).FirstAsync();
            var res2 = _db.Platos.Remove(res);
            _db.WorkersLog.Add(new WorkersLog()
            {
                IpAddress = GetClientIP(),
                LastLogin = DateTime.UtcNow,
                Location = "DeleteDish: " + dishid,
                WorkerId = 3
            });
            _db.SaveChanges();
            return new OkObjectResult(res2.Entity);
        }

        [HttpGet]
        [Route("DeleteReservation/{reservationid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteReservation(decimal reservationid)
        {
            try { 
            List<RoomsXReservation> res = _db.RoomsXReservation.Where(x => x.ReservationId == reservationid).ToList();
            res.ForEach(reservation =>
            {
                _db.RoomsXReservation.Remove(reservation);
                _db.SaveChanges();
            });
            Reservation res2 = _db.Reservation.Where(x => x.ReservationId == reservationid).First();
            _db.Reservation.Remove(res2);
            _db.SaveChanges();
            _db.WorkersLog.Add(new WorkersLog()
            {
                IpAddress = GetClientIP(),
                LastLogin = DateTime.UtcNow,
                Location = "DeleteReservation: " + reservationid,
                WorkerId = 3
            });
            return new OkObjectResult("ok");
            } catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetUsers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetUsers()
        {
            List<Users> res = _db.Users.ToList();
            return new OkObjectResult(res);
        }


        [HttpGet]
        [Route("GetKPIS/{workerid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetKPIS(decimal workerid)
        {
            var res3 = _db.Reservation.Where(x => x.WorkerId == workerid).ToList();
            KPIDTO res = new KPIDTO
            {
                owndishsellscount = _db.VentasXPlato.Where(x => x.SellerId == workerid).Count(),
                owndishsellsmoney = _db.VentasXPlato.Include(x => x.Dish).Where(x => x.SellerId == workerid).Select(x => x.Dish.Price).Sum(),
                ownreservesellscount = _db.Reservation.Where(x => x.WorkerId == workerid).Count(),
                ownreservesellsmoney = _db.Reservation.Where(x => x.WorkerId == workerid).Select(x => x.ReservationTotal).Sum(),
                todayregisteredusers = _db.Users.Where(x => x.RegisterDate.Value.DayOfYear == DateTime.Now.DayOfYear).Count(),
                totalusers = _db.Users.Count(),
                totaldishsells = _db.VentasXPlato.Include(x => x.Dish).Select(x => x.Dish.Price).Sum(),
                totaldishsellscount = _db.VentasXPlato.Count(),
                totalreservesellscount = _db.Reservation.Count(),
                todaytotalreservesellscount = _db.Reservation.Where(x => x.SellDate.Value.DayOfYear == DateTime.Now.DayOfYear).Count(),
                todaytotalreservesellsmoney = _db.Reservation.Where(x => x.SellDate.Value.DayOfYear == DateTime.Now.DayOfYear).Select(x => x.ReservationTotal).Sum(),
                todaytotaldishsellscount = _db.VentasXPlato.Where(x => x.SellDate.Value.DayOfYear == DateTime.Now.DayOfYear).Count(),
                todaytotaldishsellsmoney = _db.VentasXPlato.Include(x=> x.Dish).Where(x => x.SellDate.Value.DayOfYear == DateTime.Now.DayOfYear).Select(x => x.Dish.Price).Sum(),
                totalreservesellsmoney = _db.Reservation.Select(x => x.ReservationTotal).Sum(),
                todayroomused = _db.Reservation.Include(x => x.RoomsXReservation)
                                                .Where(x => DateTime.UtcNow.DayOfYear >= x.ReservationStart.Value.DayOfYear && DateTime.UtcNow.DayOfYear <= x.ReservationEnd.Value.DayOfYear )
                                                .Count()
            };
            _db.WorkersLog.Add(new WorkersLog()
            {
                IpAddress = GetClientIP(),
                LastLogin = DateTime.UtcNow,
                Location = "GetKPIs",
                WorkerId = workerid
            });
            _db.SaveChanges();
            return new OkObjectResult(res);
        }


        [HttpPost]
        [Route("Register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Register([FromBody] RegisterDTO model)
        {
            try
            {
                var res = await _db.Users.AddAsync(new Users()
                {
                    Email = model.email,
                    Password = model.password,
                    Name1 = model.name1,
                    Name2 = model.name2,
                    LastName1 = model.lastName1,
                    LastName20 = model.lastName20,
                    Address = model.address,
                    Phone = model.phone,
                    BussinessName = model.bussinessname,
                    RegisterDate = DateTime.UtcNow
                });
                await _db.SaveChangesAsync();
                Users user = await _db.Users.Where(x => x.UserId == res.Entity.UserId).FirstOrDefaultAsync();
                if (user == null)
                {
                    return new BadRequestObjectResult("Bad User");
                }
                return new OkObjectResult(user);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new BadRequestObjectResult("Couldn't add user, Error:" + ex.Message);
            }
        }



        [HttpPost]
        [Route("MakeReservation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> MakeReservation([FromBody] MakeReservationDTO model)
        {
            Users user = _db.Users.Where(x => x.UserId == model.userid).FirstOrDefault();
            if (user == null)
            {
                return new BadRequestObjectResult("Bad Credentials");
            }
            try
            {
                user.Reservation.Add(new Reservation()
                {
                    UserId = user.UserId,
                    WorkerId = model.workerid,
                    ReservationStart = new DateTime(model.startyear, model.startmonth + 1, model.startday),
                    ReservationEnd = new DateTime(model.endyear, model.endmonth + 1, model.endday),
                    ReservationTotal = model.total,
                    SellDate = DateTime.UtcNow,
                    ServicesXReservation = new List<ServicesXReservation>() { }
                });
                _db.SaveChanges();


                List<RoomsXReservation> toInsert = new List<RoomsXReservation>();
                var LastReservId = user.Reservation.Last().ReservationId;
                foreach (int roomid in model.rooms)
                {
                    toInsert.Add(new RoomsXReservation()
                    {
                        RoomId = roomid,
                        ReservationId = LastReservId
                    });
                }
                _db.RoomsXReservation.AddRange(toInsert);
                _db.SaveChanges();
                return new OkObjectResult(new { message = "ok" });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }
        }

        [HttpPost]
        [Route("CreateDish")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateDish([FromBody] CreateDishDTO model)
        {
            try
            {
                var res = await _db.Platos.AddAsync(new Platos
                {
                    Class = model.Class,
                    Name = model.Name,
                    Price = model.Price
                });
                await _db.SaveChangesAsync();
                _db.WorkersLog.Add(new WorkersLog()
                {
                    IpAddress = GetClientIP(),
                    LastLogin = DateTime.UtcNow,
                    Location = "CreateDish: " + model.Name,
                    WorkerId = 0
                });
                _db.SaveChanges();
                return new OkObjectResult("Dish added successfully" + res.Entity.DishId);
            } catch (Exception ex)
            {
                return new BadRequestObjectResult("Error: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDishes")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetDishes()
        {
            try
            {
                List<Platos> res = await _db.Platos.ToListAsync();
                return new OkObjectResult(res);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Error: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetDishesSellHistory")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetDishesSellHistory()
        {
            try
            {
                var res = _db.VentasXPlato.Include(x => x.Seller).Include(y => y.Dish).Select(x => new { 
                    plato = x.Dish.Name, 
                    vendedor = x.Seller.Name1,
                    fecha = x.SellDate,
                    precio = x.Dish.Price
                    });
                return new OkObjectResult(res);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Error: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetRooms")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetRooms()
        {
            try
            {
                List<Habitaciones> res = await _db.Habitaciones.ToListAsync();
                return new OkObjectResult(res);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Error: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetReservations")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetReservations()
        {
            try
            {
                var res = _db.Reservation.Include(x => x.Worker).Include(y => y.User).OrderByDescending(q => q.ReservationId).Select(x => new {
                    usuario = x.User.BussinessName,
                    vendedor = x.Worker.Name1,
                    fecha = x.SellDate,
                    total = x.ReservationTotal,
                    id = x.ReservationId,
                });
                return new OkObjectResult(res);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Error: " + ex.Message);
            }
        }


        [HttpPost]
        [Route("CreateRoom")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateRoom([FromBody] CreateRoomDTO model)
        {
            try
            {
                var res = await _db.Habitaciones.AddAsync(new Habitaciones
                {
                    Bathrooms = model.Bathrooms,
                    Beds = model.Beds,
                    IsAvailible = model.IsAvailible,
                    Name = model.Name,
                    Number = model.Number,
                    Price = model.Price
                });
                await _db.SaveChangesAsync();
                _db.WorkersLog.Add(new WorkersLog()
                {
                    IpAddress = GetClientIP(),
                    LastLogin = DateTime.UtcNow,
                    Location = "CreateRoom: " + model.Name,
                    WorkerId = 3
                });
                _db.SaveChanges();
                return new OkObjectResult("Room added successfully" + res.Entity.RoomId);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Route("SellDish")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SellDish([FromBody] SellDishDTO model)
        {
            try
            {
                var res = await _db.VentasXPlato.AddAsync(new VentasXPlato
                {
                    DishId = model.dishid,
                    SellerId = model.sellerId,
                    SellDate = DateTime.UtcNow
                });
                await _db.SaveChangesAsync();
                _db.WorkersLog.Add(new WorkersLog()
                {
                    IpAddress = GetClientIP(),
                    LastLogin = DateTime.UtcNow,
                    Location = "SellDish: " + model.dishid,
                    WorkerId = model.sellerId
                });
                _db.SaveChanges();
                return new OkObjectResult("Sell successfully: " + res.Entity.DishSellId);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Error: " + ex.Message);
            }
        }

    }
}
